export default [
    'pronoun_subject',
    'before_verb',
    'after_noun_1',
    'after_noun_2',
    'after_verb_1',
    'after_verb_2',
    'after_verb_3',
    'pronoun_object',
    '2nd_person',
    'possessive_pronoun',
    'reflexive',
];
