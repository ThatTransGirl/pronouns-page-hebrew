module.exports = [
    {code: 'de',  name: 'Deutsch',                     url: 'https://de.pronouns.page',    published: true,       symbol: 'ß',       family: 'germanic'},
    {code: 'es',  name: 'Español',                     url: 'https://pronombr.es',         published: true,       symbol: 'ñ',       family: 'romance'},
    {code: 'eo',  name: 'Esperanto',                   url: 'https://eo.pronouns.page',    published: false,      symbol: 'ĥ',       family: 'constructed'},
    {code: 'en',  name: 'English',                     url: 'https://en.pronouns.page',    published: true,       symbol: 'þ',       family: 'germanic'},
    {code: 'et',  name: 'Eesti keel',                  url: 'https://et.pronouns.page',    published: true,       symbol: 'õ',       family: 'finnish'},
    {code: 'fr',  name: 'Français',                    url: 'https://pronoms.fr',          published: true,       symbol: 'ç',       family: 'romance'},
    {code: 'gl',  name: 'Galego',                      url: 'https://gl.pronouns.page',    published: false,      symbol: 'ñ',       family: 'romance'}, // symbol duplicate with spanish
    {code: 'he',  name: 'עברית',                       url: 'https://he.pronouns.page',   published: false,      symbol: 'ע',       family: 'semitic'}, // not entirely sure about the languange family
    {code: 'it',  name: 'Italiano',                    url: 'https://it.pronouns.page',    published: false,      symbol: 'à',       family: 'romance'},
    {code: 'lad', name: 'Ladino', extra: 'Djudezmo',   url: 'https://lad.pronouns.page',   published:  true,      symbol: 'ny',      family: 'romance'},
    {code: 'nl',  name: 'Nederlands',                  url: 'https://nl.pronouns.page',    published: true,       symbol: 'ĳ',       family: 'germanic'},
    {code: 'no',  name: 'Norsk', extra: 'Bokmål',      url: 'https://no.pronouns.page',    published: true,       symbol: 'æ',       family: 'germanic'}, // å might be better, but it's used for swedish
    {code: 'pl',  name: 'Polski',                      url: 'https://zaimki.pl',           published: true,       symbol: 'ą',       family: 'slavic'},
    {code: 'pt',  name: 'Português',                   url: 'https://pt.pronouns.page',    published: true,       symbol: 'ã',       family: 'romance'},
    {code: 'ro',  name: 'Română',                      url: 'https://ro.pronouns.page',    published: true,       symbol: 'ă',       family: 'romance'},
    {code: 'sv',  name: 'Svenska',                     url: 'https://sv.pronouns.page',    published: true,       symbol: 'å',       family: 'germanic'},
    {code: 'tr',  name: 'Türkçe',                      url: 'https://tr.pronouns.page',    published: false,      symbol: 'ş',       family: 'turkic'},
    {code: 'vi',  name: 'Tiếng Việt',                  url: 'https://vi.pronouns.page',    published: true,       symbol: 'ớ',       family: 'vietic'},
    {code: 'ar',  name: 'العربية', extra: 'الفصحى',    url: 'https://ar.pronouns.page',    published: false,      symbol: 'ش',       family: 'semitic'},
    {code: 'ru',  name: 'Русский',                     url: 'https://ru.pronouns.page',    published: true,       symbol: 'й',       family: 'slavic'},
    {code: 'ua',  name: 'Українська',                  url: 'https://ua.pronouns.page',    published: true,       symbol: 'ї',       family: 'slavic'},
    {code: 'ja',  name: '日本語',                       url: 'https://ja.pronouns.page',    published: true,       symbol: 'の',      family: 'japonic'},
    {code: 'ko',  name: '한국어',                        url: 'https://ko.pronouns.page',    published: false,      symbol: '인',      family: 'koreanic'},
    {code: 'yi',  name: 'ייִדיש',                       url: 'https://yi.pronouns.page',    published: false,      symbol: 'ש',       family: 'germanic'},
    {code: 'zh',  name: '中文',                         url: 'https://zh.pronouns.page',    published: true,       symbol: '人',      family: ''},
    {code: 'tok', name: 'toki pona',                   url: 'https://tok.pronouns.page',   published: false,      symbol: '⊡',       family: 'constructed'},
];
