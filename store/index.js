import translator from '../src/translator';
import {buildDict, parseUserJwt} from "../src/helpers";

export const state = () => ({
    token: null,
    user: null,
    preToken: null,
    spelling: null,
    darkMode: false,
    translationModeVisible: false,
    translationMode: false,
    translationChanges: {},
    adPlaceholdersVisible: false,
    reducedItems: false,
    accounts: {},
})

export const mutations = {
    setToken(state, token) {
        if (!token) {
            state.token = null;
            state.user = null;
            return;
        }

        const user = parseUserJwt(token);

        if (user && user.mfaRequired) {
            state.preToken = token;
        }

        if (user && user.authenticated) {
            state.preToken = null;
            state.token = token;
            state.user = user;
            return;
        }

        state.token = null;
        state.user = null;
    },
    cancelMfa(state) {
        state.preToken = null;
    },
    setSpelling(state, spelling) {
        state.spelling = spelling;
    },
    setDarkMode(state, isDark) {
        state.darkMode = isDark;
    },
    showTranslationMode(state) {
        state.translationModeVisible = true;
    },
    translationInit(state) {
        state.translationMode = true;
    },
    translationCommit(state) {
        state.translationMode = false;
        state.translationChanges = {};
    },
    translationAbort(state) {
        state.translationMode = false;
        state.translationChanges = {};
    },
    translationPause(state) {
        state.translationMode = false;
    },
    translate(state, {key, newValue}) {
        if (newValue !== translator.get(key)) {
            const translationChanges = {...state.translationChanges};
            translationChanges[key] = newValue;
            state.translationChanges = translationChanges;
        } else {
            state.translationChanges = buildDict(function* (that) {
                for (let k in that) {
                    if (!that.hasOwnProperty(k)) { continue; }
                    if (k !== key) {
                        yield [k, that[k]];
                    }
                }
            }, state.translationChanges);
        }
    },
    restoreTranslations(state, translations) {
        if (translations) {
            state.translationMode = true;
            state.translationChanges = translations;
        } else {
            state.translationMode = false;
            state.translationChanges = {};
        }
    },
    toggleAdPlaceholdersVisible(state) {
        state.adPlaceholdersVisible = !state.adPlaceholdersVisible;
    },
    setReducedItems(state, value) {
        state.reducedItems = value;
    },
    setAccounts(state, accounts) {
        state.accounts = accounts;
    },
}
