export default {
    'support': null, // {slotId: 2557549454, adFormat: 'auto', responsive: true},

    'aside-left-top': null,
    'aside-left-middle': {slotId: '15311'}, // prev: {slotId: 8917042232, adFormat: 'autorelaxed'}, // prev: {slotId: 4999879969, adFormat: 'auto', responsive: true},
    'aside-left-bottom': null,

    'aside-right-top': null,
    'aside-right-middle': {slotId: '15312'}, // prev: {slotId: 2369847195, adFormat: 'auto', responsive: true},
    'aside-right-bottom': null,  // prev: {slotId: 6664353412, adFormat: 'auto', responsive: true},

    'main-0': {slotId: '15597'}, // video: true // prev: {slotId: 3137058361, adFormat: 'auto', adLayout: 'in-article'}, // prev: {slotId: 8172838213, adFormat: 'auto', responsive: true},
    'main-1': {slotId: '15306'}, //{slotId: 3299823474, adFormat: 'auto', responsive: true},
    'main-2': {slotId: '15309'}, // {slotId: 3108251782, adFormat: 'auto', responsive: true},
    'main-3': {slotId: '15310'}, // {slotId: 6716873048, adFormat: 'auto', responsive: true},
    'main-4': null,
    'main-5': null,
    'main-6': null,

    'video': {slotId: '15372'},

    'footer': {slotId: '15307'}, // prev: {slotId: 6584462360, adFormat: 'autorelaxed'},

    'small-homepage': {slotId: '15306'}, //{slotId: 6146027401, adFormat: 'auto', responsive: true},
};
