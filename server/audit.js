import SQL from 'sql-template-strings';
import {ulid} from "ulid";

export default async (req, event, payload = null) => {
    try {
        const user = req.user || req.rawUser || {id: null, username: null};
        await req.db.get(SQL`INSERT INTO audit_log (id, userId, username, event, payload) VALUES (
            ${ulid()}, ${user.id}, ${user.username}, ${event}, ${payload ? JSON.stringify(payload) : null}
        )`);
    } catch (e) {
        console.error(e);
    }
}
