require('../src/dotenv')();
const {decodeTime} = require('ulid');
const dbConnection = require('./db');
const SQL = require('sql-template-strings');

(async () => {
    const db = await dbConnection();

    let i = 0;
    for (let {id} of await db.all(SQL`SELECT id FROM users WHERE lastActive IS NULL`)) {
        await db.get(SQL`UPDATE users SET lastActive = ${decodeTime(id)} WHERE id = ${id}`);
        i++;
        if (i % 1000 === 0) { console.log(i); }
    }
    console.log(i);
})();
