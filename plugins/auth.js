import Vue from 'vue';
import config from '../data/config.suml';
import {isGranted, parseUserJwt} from "../src/helpers";
import cookieSettings from "../src/cookieSettings";

export default ({app, store}) => {
    const token = app.$cookies.get('token');
    if (token) {
        store.commit('setToken', token);
        if (!store.state.token) {
            app.$cookies.removeAll();
        }
    }

    Vue.prototype.$user = _ => store.state.user;
    Vue.prototype.$isGranted = (area = '', locale = null) => {
        return store.state.user
            && store.state.user.authenticated
            && isGranted(store.state.user, locale || config.locale, area)
        ;
    }

    const getAccounts = (fallback = null) => {
        const tokens = (window.localStorage.getItem('account-tokens') || fallback || '').split('|').filter(x => !!x);
        const accounts = {};
        for (let token of tokens) {
            const account = parseUserJwt(token);
            if (account.username && account.authenticated) {
                accounts[account.username] = {token, account};
            }
        }
        return accounts;
    }
    const saveAccounts = (accounts) => {
        store.commit('setAccounts', accounts);
        window.localStorage.setItem('account-tokens', Object.values(accounts).map(x => x.token).join('|'));
    }

    Vue.prototype.$accounts = () => {
        saveAccounts(getAccounts(store.state.token));
    }
    Vue.prototype.$setToken = (token) => {
        const accounts = getAccounts();

        const usernameBefore = store.state.user?.username;

        store.commit('setToken', token);
        if (token) {
            const account = parseUserJwt(token);
            if (account.username && account.authenticated) {
                accounts[account.username] = {token, account};
            }
            app.$cookies.set('token', store.state.token, cookieSettings);
        } else {
            app.$cookies.remove('token');
        }
        saveAccounts(accounts);

        const usernameAfter = store.state.user?.username;

        if (usernameBefore !== usernameAfter) {
            const bc = new BroadcastChannel('account_switch');
            bc.postMessage(usernameAfter);
        }
    };
    Vue.prototype.$removeToken = (username = null) => {
        const accounts = getAccounts();

        if (store.state.user) {
            delete accounts[username || store.state.user.username];
        }
        if (!username) {
            if (Object.keys(accounts).length === 0) {
                store.commit('setToken', null);
                app.$cookies.removeAll();
            } else {
                store.commit('setToken', Object.values(accounts)[0].token);
                app.$cookies.set('token', store.state.token, cookieSettings);
            }
        }
        saveAccounts(accounts);
    };
}
